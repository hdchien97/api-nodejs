# About Project
This project is open. My first NodeJs API build with Express Framework.
Please do not change the project information if you use it! Thank you!

## Installation
After clone this git install it:
```
yarn install
```

## Usage
Run dev: 
```
yarn run dev
```

Build and Serve
```
yarn run build && yarn run serve
```
Or: 
```
yarn run start
```

## COntributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please send me your name to add into prject information!

## License
UNLICENSED

## Contact
My Email:
```
hoangduykhanh21@gmail.com
```

My Skype
```
live:hoangduykhanh21
```
## Project Status
In process