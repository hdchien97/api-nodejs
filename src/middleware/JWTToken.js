"use strict"

import jsonwebtoken from 'jsonwebtoken';
class JWTToken {
    createJwt = async (user) => {
        return jsonwebtoken.sign({
                email: user.email,
                userName: user.userName
            },
            process.env.JWT_SECRET_KEY, {
                expiresIn: '1d',
                algorithm: 'HS512'
            });
    }
    verifyJWT = async (token) => await jsonwebtoken.verify(token, process.env.JWT_SECRET_KEY);
}
export default new JWTToken();