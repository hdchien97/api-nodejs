import formatResponse from '../middleware/formatResponse';
import JWTToken from './JWTToken';

const auth = async (req, res, next) => {
    try {
        let token = req.headers['x-access-token'] || req.headers['authorization'] || req.query.token || req.body.token;
        if (!token) {
            const message = `Request is not contain Auth token`;
            return res.status(403).send(formatResponse(null, message));
        }
        if (token.startsWith('Bearer ')) {
            token = token.slice(7, token.length);
        }
        const resultVerify = await JWTToken.verifyJWT(token);
        if (!resultVerify) {
            throw Error(`jwt not working.`)
        }
        next();
    } catch (error) {
        return res.status(403).send(formatResponse(null, error.message));
    }
}
export default auth