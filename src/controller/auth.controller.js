"use strict"

import {
    getDb
} from "../db/connectDb";
import bcrypt from 'bcrypt';
import formatResponse from '../middleware/formatResponse';
import JWTToken from '../middleware/JWTToken';
const collection = 'users';

class AuthController {
    login = async (req, res, next) => {
        try {
            const body = req.body;
            const user = await getDb().collection(collection).findOne({
                $or: [{
                    userName: body.userName
                }, {
                    email: body.email
                }]
            });
            if (!user) {
                const message = `Invalid UserName or Email`;
                return res.status(404).send(formatResponse(null, message));
            }
            const matchPassword = await bcrypt.compare(body.password, user.password);
            if (!matchPassword) {
                const message = `Unauthorized Access. Invalid Password`;
                return res.status(401).send(formatResponse(null, message));
            }
            if (matchPassword) {
                const message = `Welcome! ${user.userName}`;
                const access_token = await JWTToken.createJwt(user);
                await getDb().collection(collection).updateOne({
                    $or: [{
                        userName: body.userName
                    }, {
                        email: body.email
                    }]
                }, {
                    $set: {
                        access_token: access_token
                    }
                });
                return res.status(200).send(formatResponse({
                    token: access_token
                }, message));
            } else {
                const message = `Unauthorized Access.`;
                return res.status(401).send(formatResponse(null, message));
            }
        } catch (error) {
            const message = (error && error.message) ? error.message : `Server Error.`;
            return res.status(500).send(formatResponse(null, message));
        }
    }
}

export default new AuthController();