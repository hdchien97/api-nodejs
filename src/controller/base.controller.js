"use strict"

class BaseController {
    
    createOne = async (db, data) => {
        return await db.insertOne(data);
    }

    replace = async (req, res, next) => {

    }
    update = async (req, res, next) => {

    }
    delete = async (req, res, next) => {

    }
}
export default new BaseController();