'use strict'

import BaseController from './base.controller';
import bcrypt from 'bcrypt';
import {
    getDb,
    getPrimaryKey
} from '../db/connectDb';
import {
    UserModel
} from '../model/user.model';
import formatResponse from '../middleware/formatResponse';
import validateSchema from '../middleware/validateSchema';

const collection = 'users';
import Config from '../config';
const saltRounds = Config.saltRounds;

class UserController {
    checkExistEmail = async (req, res) => {
        try {
            const body = req.body;
            const user = await getDb().collection(collection).findOne({
                email: body.email
            });
            if (user) {
                const result = {
                    isExist: true
                };
                return res.status(200).send(formatResponse(result, 'Email already exists.'))
            } else {
                const result = {
                    isExist: false
                };
                return res.status(200).send(formatResponse(result, 'Available'))
            }
        } catch (error) {
            return res.status(500).send(formatResponse(null, 'Network Error!'))
        }
    }
    checkExistUserName = async (req, res) => {
        try {
            const body = req.body;
            const user = await getDb().collection(collection).findOne({
                userName: body.userName
            });
            if (user) {
                const result = {
                    isExist: true
                };
                return res.status(200).send(formatResponse(result, 'UserName already exists.'))
            } else {
                const result = {
                    isExist: false
                };
                return res.status(200).send(formatResponse(result, 'Available'))
            }
        } catch (error) {
            return res.status(500).send(formatResponse(null, 'Network Error!'))
        }
    }

    create = async (req, res) => {
        try {
            const userInput = req.body;
            const validate = await validateSchema(userInput, UserModel);
            const hashPass = await bcrypt.hash(validate.password, saltRounds);
            if (!hashPass) {
                return res.status(500).send(formatResponse(null, 'Server Error!'))
            }
            const saveUser = {
                ...validate,
                password: hashPass
            };
            const result = await getDb().collection(collection).insertOne(saveUser);
            if (!result) {
                return res.status(500).send(formatResponse(null, 'Server Error!'))
            }
            let {
                password,
                ...responseData
            } = {
                ...result.ops[0]
            };
            return res.status(200).send(formatResponse(responseData, 'Create Success!'));
        } catch (error) {
            res.status(500).send(formatResponse(null, error.message))
        }
    }

    findAll = async (req, res) => {
        try {
            const results = await getDb().collection(collection).find({}, {
                fields: {
                    password: 0,
                    access_token: 0
                }
            }).sort({
                createAt: 1
            }).toArray();
            if (!results) {
                return res.status(500).send(formatResponse(null, 'Server Error.'))
            }
            res.status(200).send(formatResponse(results, 'Request Success!'));
        } catch (error) {
            return res.status(500).send(formatResponse(null, error.message))
        }
    }

    findOne = async (req, res) => {
        try {
            const id = req.params.id;
            const result = await getDb().collection(collection).findOne({
                _id: getPrimaryKey(id)
            }, {
                fields: {
                    password: 0,
                    access_token: 0
                }
            });
            if (!result) {
                const message = 'User not found.';
                return res.status(404).send(formatResponse(null, message));
            }
            if (result) {
                const message = 'Request Successfully';
                return res.status(200).send(formatResponse(result, message));
            } else {
                const message = 'User not found.';
                return res.status(404).send(formatResponse(null, message));
            }
        } catch (error) {
            const message = (error && error.message) ? error.message : `Server Error.`;
            return res.status(500).send(formatResponse(null, message));
        }
    }

    findOneGraph = async (req, res) => {
        try {
            const id = req.params.id;
            const model = req.body;
            console.log(model);
            const data = await getDb().collection(collection).findOne({
                _id: getPrimaryKey(id)
            });
            res.status(200).send(formatResponse(data, 'Request OK!'));
        } catch (error) {

        }
    }
}

export default new UserController();