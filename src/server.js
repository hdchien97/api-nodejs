'use strict'

import 'dotenv/config';

import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import logger from 'morgan';

const app = express();

const environment = process.env.NODE_ENV;
import Config from './config';
const stage = Config[environment];

app.use(cors());
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(bodyParser.json())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.get('/', (req, res) => {
    res.send({
        message: 'Welcome to API server CoderTapSu'
    })
});

import router from './routes/api-routes';

app.use('/api', router);

if (environment !== 'production') {
    app.use(logger('dev'));
}

import {
    connect
} from './db/connectDb';

connect((err) => {
    if (err) {
        console.log('unable to connect to database');
        process.exit(1);
    } else {
        app.listen(parseInt(stage.port), stage.host, () => {
            console.log(`Server is running on ${stage.host}:${parseInt(stage.port)}`)
        })
    }
});